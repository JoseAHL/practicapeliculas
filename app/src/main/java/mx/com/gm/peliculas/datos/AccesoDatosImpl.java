package mx.com.gm.peliculas.datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import mx.com.gm.peliculas.domain.Pelicula;
import mx.com.gm.peliculas.excepciones.EscrituraDatosEx;

public class AccesoDatosImpl implements IAccesoDatos {

    @Override
    public void borrar(String nombreArchivo) {
        new File(nombreArchivo).delete(); 
    }

    @Override
    public void buscar(String nombreArchivo, String buscar) {
        int lineasTotales = 0;
        int totalCoincidencias = 0;
        File file = new File(nombreArchivo);
        try {

            BufferedReader leerArchivo = new BufferedReader(new FileReader(file));
            String lineaLeida;

            while ((lineaLeida = leerArchivo.readLine()) != null) {
                lineasTotales = lineasTotales + 1;

                String[] palabras = lineaLeida.split(" ");

                for (int i = 0; i < palabras.length; i++) {
                    if (palabras[i].equalsIgnoreCase(buscar)) {
                        totalCoincidencias = totalCoincidencias + 1;
                        System.out.println("Pelicula encontrada");
                        System.out.println(lineaLeida);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error" + e.getMessage());
        }
    }

    @Override
    public void crearArchivo(String nombreArchivo) {
        Path archivo = Paths.get(nombreArchivo);

        try {
            Files.createFile(archivo);
            System.out.println("Archivo creado con exito...");
        } catch (IOException e) {
            throw new EscrituraDatosEx(1, "Error al crear el archivo...");
        }
    }

    @Override
    public void escribirArchivo(Pelicula pelicula, String nombreArchivo, boolean anexar) {
        File file = new File(nombreArchivo);
        try (PrintWriter writer = new PrintWriter(new FileWriter(file, true))) {
            writer.println(pelicula.getNombre());
            System.out.println("Se agrego la pelicula en el archivo...");
        } catch (IOException e) {
            System.out.println("Erro al escribir en el archivo: " + e.getMessage());
        }
    }

    @Override
    public boolean existe(String nombreArchivo) {
        return new File(nombreArchivo).exists();
    }

    @Override
    public void listarArchivo(String nombreArchivo) {
        Path archivo = Paths.get(nombreArchivo);
        try {
            String contenido = new String(Files.readAllBytes(archivo));
            System.out.println(contenido);
        } catch (Exception e) {
            System.out.println("Error" + e.getMessage());
        }
    }
}
