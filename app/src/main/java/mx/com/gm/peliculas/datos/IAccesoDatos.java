package mx.com.gm.peliculas.datos;

import mx.com.gm.peliculas.domain.Pelicula;

public interface IAccesoDatos {

    boolean existe(String nombreArchivo);

    void listarArchivo(String nombreArchivo);

    void escribirArchivo(Pelicula pelicula, String nombreArchivo, boolean anexar);

    void buscar(String nombreArchivo, String buscar);

    void crearArchivo(String nombreArchivo);

    void borrar(String nombreArchivo);  
}
