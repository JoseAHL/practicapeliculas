package mx.com.gm.peliculas;

import java.util.Scanner;

import mx.com.gm.peliculas.negocio.CatalogoPelicuasImpl;

public class CPJLaboratorioFinal {

    private static final String nombreArchivo="/home/jose/Documentos/Java/PeliculasFinal/app/src/main/resources/peliculas.txt";
    private static int opcion;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        CatalogoPelicuasImpl catalogo=new CatalogoPelicuasImpl();

        System.out.println("************Bienvenido************");
        System.out.println("Elige una opcion:");
        System.out.println("1.- Iniciar catalogo peliculas");
        System.out.println("2.- Agregar pelicula");
        System.out.println("3.- Listar peliculas");
        System.out.println("4.- Buscar pelicula");
        System.out.println("0.- Salir...");
        opcion=sc.nextInt();
        
            switch(opcion){
                case 1:
                catalogo.iniciarArchivo(nombreArchivo);
                break;
                case 2:
                Scanner nombre = new Scanner(System.in);
                System.out.println("Ingresa el nombre de la pelicula a agregar: ");
                String nombrePelicula;
                nombrePelicula=nombre.nextLine();
                catalogo.agregarPelicula(nombreArchivo, nombrePelicula);
                break;
                case 3:
                catalogo.listarPeliculas(nombreArchivo);
                break;
                case 4:
                Scanner bs=new Scanner(System.in);
                System.out.println("Ingresa el nombre de la pelicula a buscar: ");
                String buscar=bs.nextLine();
                catalogo.buscarPelicula(nombreArchivo, buscar);
                break;
                case 0:
                System.out.println("Saliendo...");
                break;
                
            }
    }
    
}
