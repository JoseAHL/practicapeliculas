package mx.com.gm.peliculas.negocio;

public interface ICatalagoPeliculas {
    void agregarPelicula(String nombreArchivo, String nombrePelicula);

    void listarPeliculas(String nombreArchivo);

    void buscarPelicula(String nombreArchivo, String buscar);

    void iniciarArchivo(String nombreArchivo);
}
