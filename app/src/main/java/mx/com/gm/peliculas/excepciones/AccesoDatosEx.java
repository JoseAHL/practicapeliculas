package mx.com.gm.peliculas.excepciones;

public class AccesoDatosEx extends RuntimeException{
    private int codeError=0;
    
    public AccesoDatosEx(String message){
        super(message);
        //System.out.println(message);
    }

    public AccesoDatosEx(int codeError, String message){
        super(message);
        this.codeError=codeError;
    }

    public int getCodeError() {
        return codeError;
    }
    
}
