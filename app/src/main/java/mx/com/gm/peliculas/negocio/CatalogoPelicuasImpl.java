package mx.com.gm.peliculas.negocio;

import mx.com.gm.peliculas.datos.AccesoDatosImpl;
import mx.com.gm.peliculas.domain.Pelicula;
import mx.com.gm.peliculas.excepciones.EscrituraDatosEx;
import mx.com.gm.peliculas.excepciones.LecturaDatosEx;

public class CatalogoPelicuasImpl implements ICatalagoPeliculas {

    @Override
    public void agregarPelicula(String nombreArchivo, String nombrePelicula) {
        Pelicula nombre = new Pelicula(nombrePelicula);
        AccesoDatosImpl acceso = new AccesoDatosImpl();
        try {
            if (acceso.existe(nombreArchivo))
                acceso.escribirArchivo(nombre, nombreArchivo, true);
        } catch (EscrituraDatosEx e) {
            System.out.println("Erro al crear el archivo: " + e.getMessage());
        }
    }

    @Override
    public void buscarPelicula(String nombreArchivo, String buscar) {
        AccesoDatosImpl access = new AccesoDatosImpl();
        try {
            if (access.existe(nombreArchivo)) {
                access.buscar(nombreArchivo, buscar);
                return;
            }
            System.out.println("No se encuentra la pelicula en el archivo...");

        } catch (LecturaDatosEx e) {
            throw new LecturaDatosEx(2, "No se encontro la pelicula");
        }
    }

    @Override
    public void iniciarArchivo(String nombreArchivo) {
        AccesoDatosImpl access = new AccesoDatosImpl();
        if (access.existe(nombreArchivo)) {
            System.out.println("El archivo ya esta iniciado");
            return;
        }
        access.crearArchivo(nombreArchivo);
    }

    @Override
    public void listarPeliculas(String nombreArchivo) {
        AccesoDatosImpl acces = new AccesoDatosImpl();

        try {
            if (acces.existe(nombreArchivo)) {
                acces.listarArchivo(nombreArchivo);
                return;
            }
            System.out.println("No se encuentran peliculas en el archivo...");

        } catch (LecturaDatosEx e) {
            System.out.println("Erro al leer el archivo: " + e.getMessage());
        }
    }
}
